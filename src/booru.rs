use serde_json::{self, Value}; // Value; serde_json::from_str(&str)
use reqwest::Client;   // Client::new()

use std::io::Read;             // .read_to_string(&mut String)
use std::collections::HashMap; // HashMap<T, T>

pub struct MoebooruAPI {
	client : Client,
	limit  : isize,
}

impl MoebooruAPI {
	pub fn new() -> MoebooruAPI {
		MoebooruAPI {
			client : Client::new(),
			limit  : 100,
		}
	}
	fn _method<T: AsRef<str>>(&mut self, method: T, params: &HashMap<&str, &str>) -> Result<Value, String> {
		let mut url = format!("https://yande.re/{}.json?", method.as_ref());
		for (key, value) in params.iter() {
			url += format!("{}={}&", key, value).as_str();
		}
		//println!("{}", url);

		for _ in 0..4 { // make four tries
			let mut response = match self.client.get(url.as_str()).send() {
				Ok(res)  => res,
				Err(err) => {println!("MoebooruAPI::_method() [{}]", err); continue;},
			};

			let mut buf = String::new();
			response.read_to_string(&mut buf)
			        .expect("Error: MoebooruAPI::_method() ");

			let data: Value = serde_json::from_str(buf.as_str())
			                              .expect("Error: MoebooruAPI::_method() [Cannot parse response as JSON]");
			return Ok(data);
		}
		return Err("Error: MoebooruAPI::_method() [Timeout]".to_string());
	}
	pub fn post_list<T: AsRef<str>>(&mut self, page: isize, tags: T) -> Value {
		let page = page.to_string();
		let limit= self.limit.to_string();
		let params: HashMap<&str, &str> = [("page", page.as_str()),
		                                   ("tags", tags.as_ref()),
		                                   ("limit", limit.as_str())]
		                                  .iter().cloned().collect();
		let res = match self._method("post", &params) {
			Ok(res)  => res,
			Err(_)   => Value::Null,
		};
		//println!("RES: {}", res);
		res
	}
}
