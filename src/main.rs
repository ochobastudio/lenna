extern crate rand;
extern crate url;
extern crate serde_json;
extern crate reqwest;
extern crate regex;
extern crate ini;
extern crate mime_guess;

mod config;
mod booru;
mod vkapi;
mod lenna;

use lenna::Lenna;

fn main() {
	let mut lenna = Lenna::new("lenna.ini");

	//let params: HashMap<&str, &str> = [("user_ids", "1")].iter().cloned().collect();
	//let data = lenna.api._method("users.get", &params).unwrap();
	loop {
		lenna.messages_handler();
	}

	//println!("{}", data);
}
