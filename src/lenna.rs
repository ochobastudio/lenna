use url::percent_encoding::{utf8_percent_encode, DEFAULT_ENCODE_SET};
use rand::{self, Rng};         // rand::thread_rng().gen_range(isize, isize)
use serde_json::{self, Value}; // Value; serde_json::from_str(&str)
use reqwest::Client;           // Client::new()
use regex::Regex;              // Regex::new(&str)

use vkapi;
use booru::MoebooruAPI;
use config::{Config, Parser};

use std::io::Read;

pub struct Lenna {
	api:     vkapi::VKAPI,
	pic_api: MoebooruAPI,
	client:  Client,
	re_word: Regex,
	re_ator: Regex,
	re_tags: Regex,
	conf:    Config,
}

impl Lenna {
	pub fn new<T: AsRef<str>>(path: T) -> Lenna {
		let conf = Parser::load_from_file(path.as_ref());
		Lenna {
			api:     vkapi::VKAPI::new(conf.token.clone(), conf.id.clone()),
			pic_api: MoebooruAPI::new(),
			client:  Client::new(),
			re_word: Regex::new(r"(\w+)").unwrap(),
			re_ator: Regex::new(r"(\w+[ая]тор(?:\s|$))").unwrap(),
			re_tags: Regex::new(r"lenna pic (.+)").unwrap(),
			conf:    conf,
		}
	}
	pub fn messages_handler(&mut self) {
		'outer: for message in self.api.get_messages() {
			//let message_id = serde_json::from_value::<isize>(message[1].clone()).expect("Error: Lenna::messages_handler() [can't parse $message_id]");
			//let flags      = serde_json::from_value::<isize>(message[2].clone()).expect("Error: Lenna::messages_handler() [can't parse $flags]");
			let peer_id    = serde_json::from_value::<isize>(message[3].clone()).expect("Error: Lenna::messages_handler() [can't parse $peer_id]").to_string();
			//let timestamp  = serde_json::from_value::<isize>(message[4].clone()).expect("Error: Lenna::messages_handler() [can't parse $timestamp]");
			let text       = serde_json::from_value::<String>(message[5].clone()).expect("Error: Lenna::messages_handler() [can't parse $text]");
			let attachments= message[6].clone();
			let text_lower = text.to_lowercase();
			let mut text_vec: Vec<String> = Vec::new();
			for word in self.re_word.captures_iter(text_lower.as_str()) {
				//println!("{}", &word[0]);
				text_vec.push(word[0].to_string());
			}
			match text_vec.get(0).unwrap_or(&String::from("")).as_str() {
				"ленна" => match text_vec.get(1).unwrap_or(&String::from("")).as_str() {
						"что"   => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("боги говорят что это:вертушка"));
							continue;
						},
						"баланс"=> {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("Ваш баланс: 0 цыганских рупий"));
							continue;
						},
						"привет"=> {
							self.hello_lenna(peer_id, &attachments);
							continue;
						},
						"бан"   => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("Те бан ска"));
							continue;
						},
						"круто" => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("Есс!"));
							continue;
						},
						"чечня" => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("Чечня круто!"));
							continue;
						},
						"палку" => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("🏑"));
							continue;
						},
						"бутылку"=>{
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("🍾"));
							continue;
						},
						"кок"   => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("кок<br>а<br>кокаеш"));
							continue;
						},
						"лоли"  => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .attach("photo279666681_456239076"));
							continue;
						},
						""      => {
							self.api.send_message(vkapi::OutMessage::new()
							                      .id(peer_id)
							                      .message("Ану оставь Ленну в покое пидар"));
							continue;
						},
						_       => (),
					},
				"леннатор"=> {
					self.api.send_message(vkapi::OutMessage::new()
					                      .id(peer_id)
					                      .message("В ПОПЕ ЭКСКАВАТОР АХАХАХАХААА"));
					continue;
				},
				"lenna"   => match text_vec.get(1).unwrap_or(&String::from("")).as_str() {
						"version"=> {
							self.api.send_message(vkapi::OutMessage::new()
							.id(peer_id)
							.message("0.0.1"));
							continue;
						},
						"pic"    => {
							let mut tags = String::new(); // TODO: Нормальную обработку спецсимволов в тегах (без амперсанда)
							for _tags in self.re_tags.captures_iter(text_lower.replace("amp;", "").replace("&", "").as_str()) {
								tags = _tags[1].to_string();
								if tags.is_empty() { // TODO: Это условие должно выполняться для тега "&"
									tags = "rating:q order:score".to_string();
								}
								//println!("{}", _tags[0].to_string());
								break;
							}
							tags = utf8_percent_encode(tags.as_str(), DEFAULT_ENCODE_SET).to_string();
							if tags.contains("loli") { // TODO: Более интеллектуальный способ детекта лолей в посте (смотреть сам пост)
								tags = tags.replace("rating:explicit", "")
											.replace("rating:e", "")
											.replace("rating:questionable", "")
											.replace("rating:q", "");
								tags += " rating:s";
							}
							self.picture_get(peer_id, tags);
							continue;
						},
						_        => (),
					},
				"привет"  => match text_vec.get(1).unwrap_or(&String::from("")).as_str() {
						"ленна" => {
							self.hello_lenna(peer_id, &attachments);
							continue;
						},
						_       => (),
					},
				"купок"   => {
					self.api.send_message(vkapi::OutMessage::new()
					                      .id(peer_id)
					                      .message("Не я"));
					continue;
				},
				_         => (),
			}

			match text.as_str() { //Это вообще нужно?
				"Привет, Ленна" => {
					self.hello_lenna(peer_id, &attachments);
					continue;
				},
				_ => (),
			}

			if self.re_ator.is_match(text_lower.as_str()) {
				for cap in self.re_ator.captures_iter(text_lower.as_str()) {
					/*
					let message = match &cap.len() {
						1 => format!("Ану оставь {} в покое пидар", &cap[0]),
						2 => format!("Ану оставь {} и {} в покое пидар", &cap[0], &cap[1]),
						_ => { 
							let ators = String()::new();
							for word in &cap[..-1] {
								ators = format!("{}, {}", ators, word)
							}
							format!("Ану оставь {} и {} в покое пидар")
							},
					}
					*/
					let message = vkapi::OutMessage::new()
					              .message(format!("Ану оставь {} в покое пидар", &cap[0]))
					              .id(peer_id);
					self.api.send_message(message);
					continue 'outer;
				}
			}
		}
	}
	fn hello_lenna(&mut self, peer_id: String, attachments: &Value) {
		let mut user_id = String::new();
		if attachments.get("from").is_some() {
			user_id = serde_json::from_value::<String>(attachments["from"].clone()).expect("Error: Lenna::hello_lenna() [can't parse $user_id]").parse().expect("Error: Lenna::hello_lenna() [can't parse $user_id]");
		} else {
			user_id = peer_id.clone();
		}
		let name = self.api.get_name(user_id);
		let message = vkapi::OutMessage::new()
		              .message(format!("Привет, {}!", name[0]))
		              .id(peer_id);
		self.api.send_message(message);
	}
	fn pictures_list(&mut self, page: isize, tags: &str) -> Option<Vec<String>> {
		if tags.is_empty() {
			println!("Tags is empty!");
			return None;
		}
		let mut list = Vec::new();
		for file in self.pic_api.post_list(page, tags).as_array().unwrap() {
			match file["file_url"].as_str() {
				Some(url) => list.push(url.to_string()),
				None  => (),
			}
		}
		if list.len() > 0 {
			return Some(list);
		}
		return None;
	}
	fn picture_get<T: AsRef<str>>(&mut self, peer_id: String, tags: T) {
		let urls = self.pictures_list(1, tags.as_ref());
		if urls.is_some() {
			let urls = urls.unwrap();
			let file_url = urls[rand::thread_rng().gen_range(0, urls.len())].clone();
			let mut file: Vec<u8> = Vec::new();
			self.client.get(file_url.as_str())
			           .send()
			           .expect("Error: Lenna::picture_get() [can't get file_url]")
			           .read_to_end(&mut file);
			let message = vkapi::OutMessage::new()
			              .message("НА!!!")
			              .id(peer_id)
			              .attach(self.api.upload_picture(file));//.expect("Error: Lenna::picture_get() [can't upload picture]");
			self.api.send_message(message);
		} else {
			let message = vkapi::OutMessage::new()
			              .message("Изображения по запросу не найдены!")
			              .id(peer_id);
			self.api.send_message(message);
		}
	}
}
