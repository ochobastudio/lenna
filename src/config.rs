use ini::Ini; // Ini::new(); Ini::load_from_file(&str)

use std::path::Path; // Path

pub struct Config {
	pub name:  String,
	pub id:    String,
	pub token: String,
}

pub struct Parser {}

impl Parser {
	pub fn write_to_file<P: AsRef<Path>>(path: P, conf: Config) {
		let mut cfg = Ini::new();
		cfg.with_section(Some("general".to_owned()))
		    .set("name", conf.name)
		    .set("id", conf.id)
		    .set("token", conf.token);
		cfg.write_to_file(path).expect("Error: Parser::write_to_file() [can't write to file]");
	}
	pub fn load_from_file<P: AsRef<Path>>(path: P) -> Config {
		match Ini::load_from_file(path.as_ref()) {
			Ok(cfg) => Config {
				name:  cfg.get_from_or(Some("general"), "name", "").to_string(),
				id:    cfg.get_from_or(Some("general"), "id", "").to_string(),
				token: cfg.get_from_or(Some("general"), "token", "").to_string(),
			},
			Err(_)  => panic!("Error: Parser::load_from_file() [file doesn't exist]"),
		}
	}
}

/*

impl Lenna {
	fn load_config<P: AsRef<str>>(&mut self, path: P) {
		self.conf = Parser::load_from_file(path.as_ref());
	}
}

*/
