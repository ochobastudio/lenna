use rand::{self, Rng};         // rand::thread_rng().gen_range(isize, isize)
use serde_json::{self, Value}; // Value; serde_json::from_str(&str)

use reqwest::{Client, Body, Response};          // Client::new(); Body::from(&str)
use reqwest::multipart::{Form, Part}; // Form::new(Body); Part::new()
use mime_guess;

use std::io::Read;                     // .read_to_string(&mut String)
use std::collections::HashMap;         // HashMap<T, T>
use std::time::{Duration, SystemTime}; // Duration::new(u64); SystemTime::now()
use std::thread::sleep;                // sleep(Duration)

pub struct OutMessage {
	message: String,
	id: String,
	attach: String,
}

impl OutMessage {
	pub fn new() -> OutMessage {
		OutMessage {
			message: String::new(),
			id: String::new(),
			attach: String::new(),
		}
	}
	pub fn message<T: Into<String>>(mut self, message: T) -> OutMessage {
		self.message = message.into();
		self
	}
	pub fn id<T: Into<String>>(mut self, id: T) -> OutMessage {
		self.id = id.into();
		self
	}
	pub fn attach<T: AsRef<str>>(mut self, attach: T) -> OutMessage {
		if self.attach.is_empty() {
			self.attach = attach.as_ref().to_string();
		} else {
			self.attach = format!("{},{}", self.attach, attach.as_ref());
		}
		self
	}
}

pub struct VKAPI {
	client   : Client,
	token    : String,
	version  : String,
	id       : String,
	//super_id : String,
	last_time: SystemTime,
	_poll_srv: String,
	_poll_key: String,
	_poll_ts : i32,
}

impl VKAPI {
	pub fn new<T: Into<String>>(token: T, id: T) -> VKAPI {
		VKAPI {
			client   : Client::new(),
			token    : token.into(),
			version  : "5.64".to_string(),
			id       : id.into(),
			//super_id : "362506819".to_string(), // TODO: Roles
			last_time: SystemTime::now(),
			_poll_srv: String::new(),
			_poll_key: String::new(),
			_poll_ts : 0,
		}
	}
	fn captcha(&self) -> &str {
		unimplemented!();
	}
	fn _method<T: AsRef<str>>(&mut self, method: T, params: &HashMap<&str, &str>) -> Result<Value, isize> {
		let mut url = format!("https://api.vk.com/method/{}?access_token={}&v={}&", method.as_ref(), self.token, self.version);
		for (key, value) in params.iter() {
			url += format!("{}={}&", key, value).as_str();
		}
		//println!("{}", url);

		for _ in 0..4 { // make four tries
			let new_time = SystemTime::now();
			let difference = new_time.duration_since(self.last_time)
			                 .expect("Error: VKAPI::_method() [SystemTime::duration_since failed]");
			if difference < Duration::from_millis(334) {
				sleep(Duration::from_millis(334)-difference);
			}

			let mut response = match self.client.get(url.as_str()).send() {
				Ok(res)  => res,
				Err(err) => {println!("VKAPI::_method() [{}]", err); continue;},
			};

			self.last_time = SystemTime::now();

			let mut buf = String::new();
			response.read_to_string(&mut buf).expect("Error: VKAPI::_method() ");

			let data: Value = serde_json::from_str(buf.as_str()).expect("Error: VKAPI::_method() [Cannot parse response as JSON]");

			if data.get("error").is_some() { // VKAPI error handler
				let error = format!("VKAPIError #{}: {}", data["error"]["error_code"], data["error"]["error_msg"]);
				println!("{}", error);
				let error_code = serde_json::from_value::<isize>(data["error"]["error_code"].clone()).unwrap();
				match error_code {
					//6  => time.sleep(0.34),
					14   => url += self.captcha(),
					_    => (),
				}
				return Err(error_code);
			}
			else {
				return Ok(data["response"].clone());
			}
		}
		return Err(-1); // timeout error
	}
	fn _longpoll_get_server(&mut self) -> (String, String, i32) {
		let res = match self._method("messages.getLongPollServer", &HashMap::new()) {
			Ok(res)  => res,
			Err(err) => panic!("Error: VKAPI::longpoll() [VKAPIError #{}]", err),
		};

		(serde_json::from_value::<String>(res["server"].clone()).unwrap(),
		 serde_json::from_value::<String>(res["key"].clone()).unwrap(),
		 serde_json::from_value::<i32>(res["ts"].clone()).unwrap(),)
	}
	fn _longpoll(&mut self) -> Value {
		let url = format!("https://{}?act=a_check&key={}&ts={}&wait=25&mode=2&version=2", self._poll_srv, self._poll_key, self._poll_ts);
		//println!("{}", url);

		let mut response = match self.client.get(url.as_str()).send() {
			Ok(res)  => res,
			Err(err) => panic!("Error: VKAPI::longpoll() [{}]", err),
		};
		let mut buf = String::new();
		response.read_to_string(&mut buf).expect("Error: VKAPI::longpoll() ");

		serde_json::from_str(buf.as_str()).expect("Error: VKAPI::longpoll() [Cannot parse response as JSON]")
	}
	pub fn get_messages(&mut self) -> Vec<Value> { //Value {
		if self._poll_srv.is_empty() {
			let longpoll_data = self._longpoll_get_server();
			self._poll_srv = longpoll_data.0;
			self._poll_key = longpoll_data.1;
			self._poll_ts  = longpoll_data.2;
		}
		loop {
			let res = self._longpoll();
			self._poll_ts = serde_json::from_value::<i32>(res["ts"].clone()).unwrap();

			if res.get("failed").is_some() { // LongPoll error handler
				match serde_json::from_value::<isize>(res["failed"].clone()).unwrap() {
					1   => (),
					2|3 => { 
						let longpoll_data = self._longpoll_get_server();
						self._poll_srv = longpoll_data.0;
						self._poll_key = longpoll_data.1;
						},
					4   => panic!("Error: VKAPI::get_messages() [incorrect longpoll version]"),
					_   => panic!("Error: VKAPI::get_messages() [unknown longpoll error]"),
				};
				continue;
			}
			if res.get("updates").is_some() { // Updates handler
				//println!("Updates: {}", res["updates"]);
				let mut messages: Vec<Value> = Vec::new();
				for event in res["updates"].as_array().expect("Error: VKAPI::get_messages() [can't deserialize longpoll updates]") {
					match serde_json::from_value::<isize>(event[0].clone()).expect("Error: VKAPI::get_messages() [can't parse longpoll event type as isize]") {
						1  => (), // Замена флагов сообщения (FLAGS:=$flags).
						2  => (), // Установка флагов сообщения (FLAGS|=$mask).
						3  => (), // Сброс флагов сообщения (FLAGS&=~$mask).
						4  => {   // Добавление нового сообщения.
							//println!("{}", event);
							let id = match event[6].get("from") {
								Some(id) => id.as_str().unwrap_or("").to_string(),
								None     => "".to_string(),
							};
							if self.id != id && event[2].as_i64().unwrap_or(0) != 35 {
								messages.push(event.clone());
							}
						}
						6  => (), // Прочтение всех входящих сообщений в $peer_id, пришедших до сообщения с $local_id.
						7  => (), // Прочтение всех исходящих сообщений в $peer_id, пришедших до сообщения с $local_id.
						8  => (), // Друг $user_id стал онлайн.
						9  => (), // Друг $user_id стал оффлайн.
						10 => (), // Сброс флагов диалога $peer_id. Только для диалогов сообществ.
						11 => (), // Замена флагов диалога $peer_id. Только для диалогов сообществ.
						12 => (), // Установка флагов диалога $peer_id. Только для диалогов сообществ.
						13 => (), // Удаление всех сообщений в диалоге $peer_id с идентификаторами вплоть до $local_id.
						51 => (), // Один из параметров (состав, тема) беседы $chat_id были изменены.
						61 => (), // Пользователь $user_id набирает текст в диалоге.
						62 => (), // Пользователь $user_id набирает текст в беседе $chat_id.
						70 => (), // Пользователь $user_id совершил звонок с идентификатором $call_id.
						80 => (), // Счетчик непрочитанных в левом меню стал равен $count.
						114=> (), // Изменились настройки оповещений. $peer_id — идентификатор чата/собеседника.
						_  => panic!("Error: VKAPI::get_messages() [unknown longpoll event type]"),
					};
				}
				return messages;
			}
		}
	}
	pub fn get_name<T: Into<String>>(&mut self, user_id: T) -> Vec<String> {
		let user_id = user_id.into();
		let params: HashMap<&str, &str> = [("user_ids", user_id.as_str())]
		                                  .iter().cloned().collect();
		let data = self._method("users.get", &params).unwrap();

		vec![serde_json::from_value::<String>(data[0]["first_name"].clone()).expect("Error: VKAPI::get_name() [can't parse $first_name]"),
		     serde_json::from_value::<String>(data[0]["last_name"].clone()).expect("Error: VKAPI::get_name() [can't parse $last_name]")]
	}
	pub fn get_online(&mut self, user_id: isize) -> Value {
		let user_id = user_id.to_string();
		let params: HashMap<&str, &str> = [("user_ids", user_id.as_str()),
		                                   ("fields"  , "online")]
		                                  .iter().cloned().collect();
		let data = self._method("users.get", &params).unwrap();
		data
	}
	pub fn send_message(&mut self, message: OutMessage) {
		let random_id = rand::thread_rng().gen_range(0, 65536).to_string();
		//println!("{}", message);
		let params: HashMap<&str, &str> = [("message", message.message.as_str()),
		                                   ("peer_id", message.id.as_str()),
		                                   ("random_id", random_id.as_str()),
		                                   ("attachment", message.attach.as_str())]
		                                   .iter().cloned().collect();
		let _ = self._method("messages.send", &params);//.unwrap();
	}
	/*
	pub fn send_message_text<T: AsRef<str>>(&mut self, peer_id: isize, message: T) {
		self.send_message_attach(peer_id, message.as_ref(), "");
	}
	pub fn send_message_attach<T: AsRef<str>>(&mut self, peer_id: isize, message: T, attach: T) {
		let peer_id   = peer_id.to_string();
		let random_id = rand::thread_rng().gen_range(0, 65536).to_string();
		//println!("{}", message);
		let params: HashMap<&str, &str> = [("message", message.as_ref()),
		                                   ("peer_id", peer_id.as_str()),
		                                   ("random_id", random_id.as_str()),
		                                   ("attachment", attach.as_ref())]
		                                   .iter().cloned().collect();
		let _ = self._method("messages.send", &params);//.unwrap();
	}
	*/
	pub fn upload_picture<T: Into<Body>>(&mut self, file: T) -> String {
		let params: HashMap<&str, &str> = HashMap::new();
		let data = self._method("photos.getMessagesUploadServer", &params)
		    .expect("Error: VKAPI::upload_picture() [can't get upload server]");

		let file = Part::new(file.into())
		           .mime(mime_guess::get_mime_type("jpg"))
		           .file_name("file.jpg");
		let file = Form::new()
		           .part("photo", file);

		let response = self.client.post(data["upload_url"].as_str().unwrap_or(""))
		                          .multipart(file)
		                          .send();
		let mut response = match response {
			Ok(res)  => res,
			Err(err) => {println!("VKAPI::upload_picture() [{}]", err); return String::from("");},
		};

		let mut buf = String::new();
		response.read_to_string(&mut buf).expect("Error: VKAPI::upload_picture() ");
		let data: Value = serde_json::from_str(buf.as_str()).expect("Error: VKAPI::upload_picture() [Cannot parse response as JSON]");

		let _photo  = data["photo"].as_str().unwrap_or("").to_string();
		let _server = data["server"].as_i64().unwrap_or(0).to_string();
		let _hash   = data["hash"].as_str().unwrap_or("").to_string();
		let params: HashMap<&str, &str> = [("photo", _photo.as_str()),
		                                   ("server", _server.as_str()),
		                                   ("hash", _hash.as_str())]
		                                   .iter().cloned().collect();
		let data = self._method("photos.saveMessagesPhoto", &params).expect("Error: VKAPI::upload_picture [can't save picture]");
		format!("photo{}_{}", data[0]["owner_id"], data[0]["id"])
	}
}
